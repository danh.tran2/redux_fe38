import React, { Component } from "react";
import Layout from "./Layout";
import BaiTapGameBauCua from "../BaiTapGameBauCua/BaiTapGameBauCua";
import BaiTapChonXeRedux from "../BaiTapChonXeRedux/BaiTapChonXeRedux";
import Content from "./Content";

export default class PropChildren extends Component {
  renderContent = () => {
    if (this.state.menuItemActive === "xe") {
      return <BaiTapChonXeRedux />;
    } else {
      return <BaiTapGameBauCua />;
    }
  };

  state = {
    menuItemActive: "",
  };

  clickMenu = (name) => {
    console.log(name);
    this.setState({
      menuItemActive: name,
    });
  };
  render() {
    return (
      <div>
        <Content
          propRender={(propTitle) => {
            return <p>{propTitle}</p>;
          }}
        ></Content>

        {/* <Layout component={<BaiTapGameBauCua />}>  */}
        {/* Truyền theo dạng component như trên vẫn được nhưng dễ rối, cách dưới sẽ ko cần truyền props component thông qua child */}

        {/* <Layout clickMenu={this.clickMenu} /> */}
        {/* Nếu viết kiểu trên sẽ khác, ko cần thẻ tag dưới và render sai ý định
        Cách dưới thì 3 thẻ bên trong ko được render và được xem là props.child, 
        cmt child trong tag Layout sẽ thấy render ra */}
        <Layout clickMenu={this.clickMenu}>
          {this.renderContent()}
          <BaiTapGameBauCua />
          <BaiTapChonXeRedux />
        </Layout>
      </div>
    );
  }
}
