import React, { Component } from "react";

export default class Layout extends Component {
  render() {
    return (
      <div>
        <div className="container">
            <div className="row">
              <div 
                className="nav flex-column nav-pills col-2"
                id="v-pills-tab"
                role="tablist"
                aria-orientation="vertical"
              >
                <a
                  className="nav-link active"
                  id="v-pills-home-tab"
                  data-toggle="pill"
                  href="#v-pills-home"
                  role="tab"
                  aria-controls="v-pills-home"
                  aria-selected="true"
                  onClick ={()=>{this.props.clickMenu('game')}}
                >
                  Game
                </a>
                <a
                  className="nav-link"
                  id="v-pills-profile-tab"
                  data-toggle="pill"
                  href="#v-pills-profile"
                  role="tab"
                  aria-controls="v-pills-profile"
                  aria-selected="false"
                  onClick ={()=>{this.props.clickMenu('xe')}}
                >
                  Xe
                </a>
              </div>
              <div className="tab-content col-10" id="v-pills-tabContent">
                {this.props.children}
              </div>
            </div>
        </div>
      </div>
    );
  }
}
