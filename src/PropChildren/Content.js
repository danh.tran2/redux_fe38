import React, { Component } from 'react'

export default class Content extends Component {
    render() {
        return (
            <div>
            {this.props.propRender('hello cybersoft')}
            {/* hoặc render truyền 1  thẻ tag vẫn được */}
            {/* có cần phải đặt là render không hay chỉ là tên tượng trưng của 1 prop func */}
            </div>
        )
    }
}
