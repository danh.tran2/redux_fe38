import { LAY_DANH_SACH_PHIM } from "../types/quanLyPhimType";

const initialState = {
  danhSachPhim: [],
};

export default (state = initialState, action) => {
  switch (action.type) {
    case LAY_DANH_SACH_PHIM:
      state.danhSachPhim = action.dsPhim;
      return { ...state };
    default:
      return state;
  }
};
