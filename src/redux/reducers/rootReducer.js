import { combineReducers } from "redux";
import stateModalReducer from "../reducers/ModalReducer";
import GioHangReducer from "../reducers/GioHangReducer";
import GameBauCuaReducer from "../reducers/GameBauCuaReducer";
import QuanLyPhimReducer from "../reducers/QuanLyPhimReducer"
// rootReducer là state (state tổng của toàn ứng dụng)
// reducer là state của redux
const rootReducer = combineReducers({
  // Nơi khai báo các state của ứng dụng
  stateModalReducer: stateModalReducer,
  // Khai báo giỏ hàng tắt theo ES6
  GioHangReducer,
  GameBauCuaReducer,
  QuanLyPhimReducer
});

export default rootReducer;
