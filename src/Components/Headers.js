import React, { Component } from "react";
import {NavLink} from "react-router-dom"

export default class Headers extends Component {
  render() {
    return (
      <div>
        <nav className="navbar navbar-expand-sm navbar-dark bg-dark">
          <NavLink className="navbar-brand" to="/">
            Fe38-React
          </NavLink>
          <button
            className="navbar-toggler d-lg-none"
            type="button"
            data-toggle="collapse"
            data-target="#collapsibleNavId"
            aria-controls="collapsibleNavId"
            aria-expanded="false"
            aria-label="Toggle navigation"
          >
            <span className="navbar-toggler-icon" />
          </button>
          <div className="collapse navbar-collapse" id="collapsibleNavId">
            <ul className="navbar-nav mr-auto mt-2 mt-lg-0">
              <li className="nav-item">
                <NavLink activeClassName="active" activeStyle={{color:'red' , backgroundColor:'white'}} className="nav-link" to="/login">
                  Login <span className="sr-only">(current)</span>
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName="active" activeStyle={{color:'red' , backgroundColor:'white'}}  className="nav-link" to="/btchonxe">
                  Products
                </NavLink>
              </li>
              <li className="nav-item">
                <NavLink activeClassName="active" activeStyle={{color:'red' , backgroundColor:'white'}}  className="nav-link" to="/propchildren">
                  Props Child
                </NavLink>
              </li>
              <li className="nav-item dropdown">
                <a
                  className="nav-link dropdown-toggle"
                  href="#"
                  id="dropdownId"
                  data-toggle="dropdown"
                  aria-haspopup="true"
                  aria-expanded="false"
                >
                  Dropdown
                </a>
                <div className="dropdown-menu" aria-labelledby="dropdownId">
                  <NavLink className="dropdown-item" to="/game">
                    Game bau cua
                  </NavLink>
                  <NavLink className="dropdown-item" to="/btchonxe">
                    Mua xe
                  </NavLink>
                </div>
              </li>
            </ul>
            <form className="form-inline my-2 my-lg-0">
              <input
                className="form-control mr-sm-2"
                type="text"
                placeholder="Search"
              />
              <button
                className="btn btn-outline-success my-2 my-sm-0"
                type="submit"
              >
                Search
              </button>
            </form>
          </div>
        </nav>
      </div>
    );
  }
}
