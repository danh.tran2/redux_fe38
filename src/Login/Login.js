import React, { Component } from "react";

export default class Login extends Component {
  login = () => {
    console.log("Đăng nhập thành công");
    alert("login success");
    // Chuyển hướng trang
    // Thuộc tính history push chuyển hướng trang cho phép browser back lại trang trước
    // this.props.history.push("/");
    // Thuộc tính history replace chuyển hướng trang không cho phép browser back lại trang trước
    this.props.history.replace("/");
  };

  render() {
    return (
      <div className="container-fluid">
        <h3 className="text-center display-4">Login</h3>
        <div className="form-group">
          <h3>Username</h3>
          <input className="form-control" />
        </div>
        <div class="form-group">
          <h3>Password</h3>
          <input type="password" className="form-control" />
        </div>
        <button
          onClick={() => {
            this.login();
          }}
          className="btn btn-primary"
        >
          Login
        </button>
      </div>
    );
  }
}
