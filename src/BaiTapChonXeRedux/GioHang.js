import React, { Component } from "react";
import { connect } from "react-redux";

class GioHang extends Component {
  renderGioHang = () => {
    return this.props.gioHang.map((sanPham, index) => {
      return (
        <tr key={index}>
          <td>{sanPham.maSP}</td>
          <td>
            <img
              src={sanPham.hinhAnh}
              alt="123"
              style={{ width: "75px", height: "75px" }}
            />
          </td>
          <td>{sanPham.tenSP}</td>
          <td>{sanPham.gia.toLocaleString()}</td>
          <td>
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.tangGiamSoLuong(sanPham.maSP, true);
              }}
            >
              +
            </button>
            {sanPham.soLuong}
            <button
              className="btn btn-primary"
              onClick={() => {
                this.props.tangGiamSoLuong(sanPham.maSP, false);
              }}
            >
              -
            </button>
          </td>
          <td>{(sanPham.gia * sanPham.soLuong).toLocaleString()}</td>
          <td>
            <button
              onClick={() => {
                this.props.xoaGioHang(sanPham.maSP);
              }}
              className="btn btn-primary"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };

  tinhTongTien = () => {
    return this.props.gioHang.reduce((tongTien, sanPham, index) => {
      return (tongTien += sanPham.gia * sanPham.soLuong);
    }, 0);
  };
  render() {
    console.log("gioHang", this.props.gioHang);
    return (
      <div className="container-fluid">
        <h3>Giỏ hàng</h3>
        <table className="table">
          <thead>
            <tr>
              <th>Mã SP</th>
              <th>Hình ảnh</th>
              <th>Tên SP</th>
              <th>Đơn giá</th>
              <th>Số lượng</th>
              <th>Thành tiền</th>
              <th></th>
            </tr>
          </thead>
          <tbody>{this.renderGioHang()}</tbody>
          <tfoot>
            <tr>
              <td colSpan="6"></td>
              <td>{this.tinhTongTien().toLocaleString()}</td>
            </tr>
          </tfoot>
        </table>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // state là RootReducer
  return {
    gioHang: state.GioHangReducer, // Chuyển state redux thành props của component
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    xoaGioHang: (maSP) => {
      // tạo action
      let action = {
        type: "XOA_GIO_HANG",
        maSP,
      };
      // Gửi action lên reducer
      dispatch(action);
    },
    // themSoLuong: (maSP) => {
    //   let action = {
    //     type: "THEM_SO_LUONG",
    //     maSP,
    //   };
    //   dispatch(action);
    // },
    // xoaSoLuong: (maSP) => {
    //   let action = {
    //     type: "XOA_SO_LUONG",
    //     maSP,
    //   };
    //   dispatch(action);

    // },
    tangGiamSoLuong: (maSP, tangGiam) => {
      dispatch({
        type: "TANG_GIAM_SL",
        maSP,
        tangGiam,
      });
    },
  };
};

// Biến component gioHang thành component giỏ hàng chứa props của redux
export default connect(mapStateToProps, mapDispatchToProps)(GioHang);
