import React, { Component } from "react";
import { connect } from "react-redux";

export class SanPham extends Component {
  render() {
    // Lấy giá trị props từ componmemt DanhSachSanPham truyền vào
    let { sanPham } = this.props;
    return (
      <div className="card text-white bg-dark">
        <img
          style={{ width: "100%" }}
          className="card-img-top"
          src={sanPham.hinhAnh}
          alt =""
        />
        <div className="card-body">
          <h4 className="card-title">{sanPham.tenSP}</h4>
          <p className="card-text">{sanPham.gia}</p>
          <button
            className="btn btn-success"
            data-toggle="modal"
            data-target="#modelId"
            onClick={() => {
              this.props.xemChiTiet(sanPham);
            }}
          >
            Xem chi tiết
          </button>
          <button
            onClick={() => {
              this.props.themGioHang(sanPham);
            }}
            className="btn btn-danger"
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}

const mapDispatchToProps = (dispatch) => {
  return {
    xemChiTiet: (sanPham) => {
      let action = {
        type: "XEM_CHI_TIET", // Thuộc tính bắt buộc
        sanPham: sanPham, // Giá trị gửi lên store
      };
      // Dùng hàm dispatch đưa action lên store
      dispatch(action);
    },

    themGioHang: (sanPham) => {
      // 1 tạo action đưa lên reducer
      let action = {
        type: "THEM_GIO_HANG", //đây là 1 nhãn để biết thao tác với state nào trên redux
        sanPhamGioHang: {
          tenSP:sanPham.tenSP,
          maSP: sanPham.maSP,
          hinhAnh: sanPham.hinhAnh,
          gia: sanPham.gia,
          soLuong: 1,
        },
      };

      // 2 dùng phương thức dispatch gửi dữ liệu lên reducer
      dispatch(action);
    },
  };
};

// SanPham chứ ko phải sanPham ở đây - tên của component
export default connect(null, mapDispatchToProps)(SanPham);
