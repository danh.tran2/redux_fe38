import React, { Component, Fragment } from "react";
import BaiTapChonXeRedux from "./BaiTapChonXeRedux/BaiTapChonXeRedux";
// import GioHang from "./BaiTapChonXeRedux/GioHang";
import BaiTapGameBauCua from "./BaiTapGameBauCua/BaiTapGameBauCua";
import BaiTapCallApi from "./ReactLifeCycle/BaiTapCallApi";
import { BrowserRouter, Route, Switch, Redirect } from "react-router-dom";
import Headers from "./Components/Headers";
import Home from "./Home/Home";
import Login from "./Login/Login";
import PropChildren from "./PropChildren/PropChildren";

export default class App extends Component {
  render() {
    return (
      <BrowserRouter>
        <Switch>
          <Fragment>
            {/* <Headers /> */}
            <Route exact path="/login" component={Login} />
            <Route
              exact
              path="/game"
              render={(props) => {
                return (
                  <Fragment>
                    <Headers />
                    <BaiTapGameBauCua {...props} />
                  </Fragment>
                );
              }}
            />
            <Route exact path="/btchonxe" component={BaiTapChonXeRedux} />
            {/* Home nên để cuối */}
            <Route exact path="/propchildren" component={PropChildren} />
            <Route exact path="/" component={Home} />
            {/* <Redirect to="/" /> */}
          </Fragment>
        </Switch>
      </BrowserRouter>
      // <div>
      //   {/* <BaiTapChonXeRedux /> */}
      //   {/* <GioHang /> */}
      //   {/* <BaiTapGameBauCua /> */}
      //   <BaiTapCallApi />
      // </div>
    );
  }
}
