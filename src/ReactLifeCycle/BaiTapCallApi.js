import React, { Component } from "react";
import axios from "axios";
import { connect } from "react-redux";
import { layDanhSachPhimAction } from "../redux/actions/quanLyPhimAction";
// import { PLAY_GAME } from "../redux/types/gameBauCuaType";

export class BaiTapCallApi extends Component {
  constructor(props) {
    super(props);
    console.log("constructor");
  }

  // componentWillMount = () => {
  //   console.log("componentWillMount");
  // };

  state = {
    danhSachPhim: [],
  };

  renderFilm = () => {
    console.log(this.props.danhSachPhim);
    return this.props.danhSachPhim.map((phim, index) => {
      return (
        <div className="col-3" key={index}>
          <div className="card text-left">
            <img src={phim.hinhAnh} alt={phim.biDanh} />
            <div className="card-body">
              <h3>{phim.tenPhim}</h3>
              <p>{phim.moTa}</p>
            </div>
          </div>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="comtainer-fluid">
        <h3>Danh sách phim</h3>
        <div className="row">{this.renderFilm()}</div>
      </div>
    );
  }

  componentDidMount = () => {
    // chạy 1 lần sau khi render component
    // axios({
    //   url:
    //     "http://movie0706.cybersoft.edu.vn/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP01", //link request
    //   method: "GET", // phương thức backend quy định cho link api
    // })
    //   .then((res) => {
    //     console.log("data", res);
    //     this.setState({ danhSachPhim: res.data });
    //   })
    //   .catch((err) => {
    //     console.log("err", err.response.data);
    //   });
    // console.log("componentDidMount");
    // call API
    this.props.LayDanhSachPhim();
  };
}

const mapStateToProps = (state) => {
  return {
    danhSachPhim: state.QuanLyPhimReducer.danhSachPhim,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    LayDanhSachPhim: () => {
      dispatch(layDanhSachPhimAction());
    },
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(BaiTapCallApi);
