import React, { Component } from "react";
import { connect } from "react-redux";
import { datCuocAction } from "../redux/actions/gameBauCuaAction";

export class DanhSachCuoc extends Component {
  renderDanhSachCuoc = () => {
    // console.log(this.props.danhSachCuoc);
    return this.props.danhSachCuoc.map((quanCuoc, index) => {
      return (
        <div className="col-4 mt-2" key={index}>
          <button
            onClick={() => {
              this.props.datCuoc(quanCuoc);
            }}
          >
            <img src={quanCuoc.hinhAnh} />
            <br />
            <span style={{ fontSize: "50px", color: "red" }}>
              {quanCuoc.diemCuoc}
            </span>
          </button>
        </div>
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">{this.renderDanhSachCuoc()}</div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // state là RootReducer
  return {
    danhSachCuoc: state.GameBauCuaReducer.danhSachCuoc, // Chuyển state redux thành props của component
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    datCuoc: (quanCuoc) => {
      dispatch(datCuocAction(quanCuoc));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DanhSachCuoc);
