import React, { Component } from "react";
import { connect } from "react-redux";
import { playGameAction, datCuocAction } from "../redux/actions/gameBauCuaAction";

export class XucXac extends Component {
  renderXucXac = () => {
    return this.props.xucXac.map((item, index) => {
      return (
        <img
          style={{ width: "50px", height: "50px" }}
          key={index}
          src={item.hinhAnh}
          alt={item.ma}
        />
      );
    });
  };

  render() {
    return (
      <div className="container">
        <div className="row">
          <div className="col-2">
            <button
              className="btn btn-success"
              onClick={() => {
                this.props.playGame();
              }}
            >
              Play
            </button>
          </div>
          <div className="col-8">
            <div className="xuc-xac text-center">
              {/* {this.props.xucXac.map((item, index) => {
                return (
                  <img
                    style={{ width: "50px", height: "50px" }}
                    key={index}
                    src={item.hinhAnh}
                    alt={item.ma}
                  />
                );
              })} */}
              {/* <img
                style={{ width: "50px", height: "50px" }}
                src="./img/gameImg/bau.PNG"
              /> */}
              {this.renderXucXac()}
            </div>
          </div>
          <div className="col-2">
            <span className="display-4">{this.props.tongTien}$</span>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  // state là RootReducer
  return {
    xucXac: state.GameBauCuaReducer.xucXac,
    tongTien: state.GameBauCuaReducer.tongTien,
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    playGame: () => {
      // xử lý random xúc xắc mới
      dispatch(playGameAction());
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(XucXac);
